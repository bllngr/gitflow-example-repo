Example Git (Flow) Repository
=============================

This file serves as an example for Git, [Atlassian's SourceTree](https://www.sourcetreeapp.com/), a branching model commonly refered to as [Git Flow workflow](https://www.sourcetreeapp.com/) and it's intergration in SourceTree.

It is used to showcase the different branches (``master``, ``develop``, ``feature/*``, ``hotfix/*`` and ``release/*``), and how they can be used to define a strict branching model designed around the project release.

What is Git Flow?
-----------------

> This workflow doesn’t add any new concepts or commands beyond what’s
> required for the Feature Branch Workflow. Instead, it assigns very
> specific roles to different branches and defines how and when they
> should interact. In addition to feature branches, it uses individual
> branches for preparing, maintaining, and recording releases. Of
> course, you also get to leverage all the benefits of the Feature
> Branch Workflow: pull requests, isolated experiments, and more
> efficient collaboration.

Source: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

Further info
------------
* [Getting Started with Git](https://www.atlassian.com/git/tutorials/setting-up-a-repository)
* [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)
* [Smart branching with SourceTree and Git-flow](http://blog.sourcetreeapp.com/2012/08/01/smart-branching-with-sourcetree-and-git-flow/)
* [Git Branching - Branches in a Nutshell](http://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
